import React from 'react';
import App from './../App';
import Details from '../views/Details';
import Homepage from '../views/Homepage';

export default [
  {
    ...App,
    routes: [
      {
        ...Homepage,
        path: '/',
        exact: true
      },
      {
        ...Details,
        path: '/detail/:id'
      },
    ]
  }
];
