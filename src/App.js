import React, { Component } from 'react';
import { renderRoutes } from 'react-router-config';
import { Link } from 'react-router-dom';
// import logo from './logo.svg';
// import './App.css';
import Title from './Title';


class App extends Component {

  componentDidMount() {
    console.log('APP:', this.props);
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          {/* <img src={logo} className="App-logo" alt="logo" /> */}
          <h1 className="App-title">
            <Link to="/">Welcome to Reactr1 FFFF</Link>
          </h1>
          <Title/>
        </header>

        <div>{renderRoutes(this.props.route.routes)}</div>
      </div>
    );
  }
}

export default {
  component: App
}

