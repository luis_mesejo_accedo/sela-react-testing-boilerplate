import React, { Component } from 'react';
import T from 'prop-types';
import { selectors, actions } from './../../redux/modules/movies';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

const mapStateToProps = state => {
  return {
    movies: selectors.movies(state)
  }
}

class Homepage extends Component {
  render() {
    const { movies } = this.props;

    if (!movies) {
      return 'loading...'
    }

    return (
      <div>
        <div>
          {
            movies.map(movie => {
              return (
                <div key={movie.id}>
                  <Link to={`detail/${movie.id}`}>
                    <h1>{movie.title}</h1>
                  </Link>
                  <img src={movie.images[0].url} alt="" />
                  <p>{movie.description}</p>
                </div>
              )
            })
          }
        </div>
      </div>
    );
  }
}

Homepage.propTypes = {
  movies: T.array
};

const loadData = ({ dispatch }) => {
  return dispatch(actions.fetchGetMovies())
}

export default {
  component: connect(mapStateToProps)(Homepage),
  loadData
}