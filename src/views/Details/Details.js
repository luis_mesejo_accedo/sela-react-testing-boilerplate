import React, { Component } from 'react';
import T from 'prop-types';
import { connect } from 'react-redux';
import { selectors } from './../../redux/modules/movies';

const makeMapStateToProps = () => {
  const getDetails = selectors.getDetails();
  const mapStateToProps = (state, props) => {
    return {
      details: getDetails(state, props)
    }
  }
  return mapStateToProps;
}

class Details extends Component {

  componentDidMount() {
    console.log('DETAIL: ', this.props);
  }


  render() {
    const {
      details: {
        description,
        title ,
        images: [ { url } = {} ] = []
      } = {}
    } = this.props
    return (
      <div>
        <h1>{title}</h1>
        <img src={url} alt=""/>
        <p>{description}</p>
      </div>
    );
  }
}

Details.propTypes = {
  title: T.string,
  cover: T.string,
  description: T.string
};

export default {
  component: connect(makeMapStateToProps)(Details)
};