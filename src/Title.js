import React from 'react';
import styled from 'styled-components';

// Create a Title component that'll render an <h1> tag with some styles
const TitleText = styled.h1`
  font-size: 60px;
  text-align: center;
  color: red;
`;

// Create a Wrapper component that'll render a <section> tag with some styles
const Wrapper = styled.section`
  padding: 4em;
  background: blue;
`;

// Use Title and Wrapper like any other React component – except they're styled!

const Title = () => (
  <Wrapper>
    <TitleText>
      Hello World, this is my first styled component!
    </TitleText>
  </Wrapper>
);

export default Title;