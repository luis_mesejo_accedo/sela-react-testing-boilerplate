import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
// import './index.css';
import Routes from './routes/Routes';
import registerServiceWorker from './registerServiceWorker';
import configureStore from './redux/store';

ReactDOM.hydrate(
  <Provider store={configureStore()}>
    <BrowserRouter>
      <div>{renderRoutes(Routes)}</div>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root'));
// registerServiceWorker();
