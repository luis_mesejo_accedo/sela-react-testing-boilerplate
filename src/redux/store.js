import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducer';


let initState;
const hasWindow = typeof window === 'object';
if (hasWindow) {
  initState = window.INITIAL_STATE;
  delete window.INITIAL_STATE;
  console.log('[store] Initial state: ', initState);
} else {
  console.log('initial state empty');
  initState = {};
}

const middlewares = [
  applyMiddleware(thunk)
];

const tools = [];
if (hasWindow && window.__REDUX_DEVTOOLS_EXTENSION__) {
  tools.push(
    window.__REDUX_DEVTOOLS_EXTENSION__()
  )
}

export default function configureStore(initialState = initState) {
  return createStore(
    reducers,
    initialState,
    compose(
      ...middlewares,
      ...tools
    )
  );
}