import axios from 'axios';

export const GET_MOVIES = 'app/GET_MOVIES';

const getMovieAction = ({ movies })  => ({
   type: GET_MOVIES,
   movies
 })

 const fetchGetMovies = () => (dispatch) => {
  return axios.get('https://demo2697834.mockable.io/movies')
  .then(resp => {
    const movies = resp.data;
    dispatch(getMovieAction({ movies }))
  })
  .catch(err => {
    // dispatch error action
  })
 }

 export default {
  fetchGetMovies
 }