import { createSelector } from 'reselect'

const movies = (state) => state.movies.items;

const getDetails = () => {
  return createSelector(
    (state, props) => props.match.params.id,
    movies,
    (id, movies) => movies.find(movie => movie.id === id)
  )
}

export default {
  movies,
  getDetails
}