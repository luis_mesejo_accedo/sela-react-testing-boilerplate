import {
  GET_MOVIES
} from './actions';

const movies =  (state = {}, action) => {
  switch (action.type) {
   case GET_MOVIES:
    return {
     items: action.movies.entries,
     count: action.movies.totalCount
    }
   default:
    return state
  }
 }
 export default movies;