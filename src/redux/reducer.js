import { combineReducers } from 'redux';
import { reducer as movies } from './modules/movies';

export default combineReducers({
  movies
});