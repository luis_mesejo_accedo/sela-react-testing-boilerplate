This project was bootstrapped with [Create React App Rewired](https://github.com/timarney/react-app-rewired).

## Install

* clone repo
* npm install

## Available Scripts

In the project directory, you can run:

### `npm start` without SSR

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.

You will also see any lint errors in the console.

### `npm run start:ssr` with SSR

If you are running the app for the first time run: `npm run dev:ssr`, it will create an update compiled version of the app.

Runs the app with a server node `src/server/index.js`.

The node server will start a server with babel.

It is is pointed to `ssr_static` folder where it is located the compliled version of the app through a webpack process described in `webpack.ssr.js`.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


The full explanation of the SSR flow can be found -link to another page-. (TBD)


#### TODO:
- add webpack loader for images
- add webpack loader for css (if needed)
- add webpack loader for fonts
- implement live reload

### `npm test` (TBD)

Launches the test runner in the interactive watch mode.


### `npm run build`

Builds the app for production to the `build` folder.

The build is minified and the filenames include the hashes.

#### TODO
- fix build for SSR managing the hashes of static files.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Folder Structure (work in progress)

After creation, your project should look like this:

```
my-app/
  README.md
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
  src/
    App.css
    App.js
    App.test.js
    index.css
    index.js
    logo.svg
```